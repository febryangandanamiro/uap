package Contest;

class PemesananTiket {
    // membuat kelas pemesanan tiket yang berisi method-method yang berfungsi untuk memesan tiket.
    private String namaPemesan;
    private TiketKonser tiket;

    //konstruktor untuk mendapat nama pemesan dan jenis tiketnya
    public PemesananTiket(String namaPemesan, TiketKonser tiket) {
        this.namaPemesan = namaPemesan;
        this.tiket = tiket;
    }
    //method untuk mendapat kode pesanan/kode booking
    public String getKodePesanan() {
        return Main.generateKodeBooking();
    }
    //method untuk mendapat tanggal pesanan
    public String getTanggalPesanan() {
        return Main.getCurrentDate();
    }
    //method untuk mendapat total harga
    public int getTotalHarga() {
        return tiket.getHarga();
    }
    //method untuk menampilkan detail pemesanan setelah pemesan memasukkan nama dan jenis tiket
    public void tampilkanDetailPesanan() {
        System.out.println("\n------------ Detail Pemesanan ------------");
        System.out.println("Nama Pemesan: " + namaPemesan);
        System.out.println("Kode Booking: " + getKodePesanan());
        System.out.println("Tanggal Pesanan: " + getTanggalPesanan());
        System.out.println("Tiket yang Dipesan: " + tiket.getNama());
        System.out.println("Total Harga: Rp " + getTotalHarga());
    }
}