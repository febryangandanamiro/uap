package Contest;
//kelas ini bertujuan untuk menampilkan pesan error yang lebih mudah dibaca dan dipahami oleh pengguna
class InvalidInputException extends Exception {
    public InvalidInputException(String message) {
        super(message);
    }
}