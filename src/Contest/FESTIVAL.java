package Contest;

class FESTIVAL extends TiketKonser {
    //membuat kelas festival dengan mendefinisikan nama dan harganya sesuai pada konstruktor di kelas induknya,
    //yaitu kelas abstrak Tiketkonser menggunakan super
    public FESTIVAL() {
        super("FESTIVAL", 4000000);
    }

    @Override
    public int getHargaTiket() {
        return 0;
    }
}