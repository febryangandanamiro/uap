package Contest;

interface HargaTiket {
    //membuat interface HargaTiket untuk diimplementasikan pada kelas turunan dari kelas abstrak TiketKonser implements HargaTiket
    int getHargaTiket();
}