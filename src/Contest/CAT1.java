package Contest;

class CAT1 extends TiketKonser {
    //membuat kelas cat1 dengan mendefinisikan nama dan harganya sesuai pada konstruktor di kelas induknya,
    //yaitu kelas abstrak Tiketkonser menggunakan super
    public CAT1() {
        super("CAT1", 6000000);
    }

    @Override
    public int getHargaTiket() {
        return 0;
    }
}