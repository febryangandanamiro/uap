package Contest;

class CAT8 extends TiketKonser {
    //membuat kelas cat8 dengan mendefinisikan nama dan harganya sesuai pada konstruktor di kelas induknya,
    //yaitu kelas abstrak Tiketkonser menggunakan super
    public CAT8() {
        super("CAT8", 900000);
    }

    @Override
    public int getHargaTiket() {
        return 0;
    }
}