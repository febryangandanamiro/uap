package Contest;

class VVIP extends TiketKonser {
    //membuat kelas vvip dengan mendefinisikan nama dan harganya sesuai pada konstruktor di kelas induknya,
    //yaitu kelas abstrak Tiketkonser menggunakan super
    public VVIP() {
        super("UNLIMITED EXPERIENCE", 11000000);
    }

    @Override
    public int getHargaTiket() {
        return 0;
    }
}