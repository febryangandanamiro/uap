/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        //membuat tampilan untuk menu pemesanan menggunakn system.outprint,
        //scanner disini untuk mengambil input dari pemensan
        Scanner scanner = new Scanner(System.in);
        System.out.println("Selamat datang di Pemesanan Tiket Coldplay!");
        System.out.println("===================================================");

        System.out.print("Masukkan nama pemesan: ");
        String namaPemesan = scanner.nextLine();

        System.out.println("Pilih jenis tiket:");
        System.out.println("1. CAT8");
        System.out.println("2. CAT1");
        System.out.println("3. Festival");
        System.out.println("4. VIP");
        System.out.println("5. UNLIMITED EXPERIENCE");
        //membuat try catch
        //dalam try terdapat switch case untuk pilihan jenis tiket yang akan dipesan
        //jika pemesan memasukkan angka yang valid, maka catch tidak akan dijalankan
        //namun jika pemesan memasukkan angka yang salah maka akan menthrow ke catch
        //kemudian di catch akan menjalankan exception untuk menampilkan pesan error
        try {
            System.out.print("Masukkan pilihan: ");
            int nomorTiket = scanner.nextInt();
            TiketKonser tiket = null;

            switch (nomorTiket) {
                case 1:
                    tiket = new CAT8();
                    break;
                case 2:
                    tiket = new CAT1();
                    break;
                case 3:
                    tiket = new FESTIVAL();
                    break;
                case 4:
                    tiket = new VIP();
                    break;
                case 5:
                    tiket = new VVIP();
                    break;
                default:
                    throw new InvalidInputException("Terjadi kesalahan: Nomor tiket yang Anda masukkan tidak valid.");
            }

            PemesananTiket pesanan = new PemesananTiket(namaPemesan, tiket);
            pesanan.tampilkanDetailPesanan();
        } catch (InvalidInputException e) {
            System.out.println(e.getMessage());
        }
    }



    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}