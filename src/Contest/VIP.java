package Contest;

class VIP extends TiketKonser {
    //membuat kelas vip dengan mendefinisikan nama dan harganya sesuai pada konstruktor di kelas induknya,
    //yaitu kelas abstrak Tiketkonser menggunakan super
    public VIP() {
        super("VIP", 8000000);
    }

    @Override
    public int getHargaTiket() {
        return 0;
    }
}