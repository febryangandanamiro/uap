package Contest;

abstract class TiketKonser implements HargaTiket {
    // membuat kelas abstrak TiketKonser yang istilahnya menjadi kerangka untuk kelas-kelas turunannya,
    // yaitu cat1,cat8,festival,vip dan vvip. Semua kelas tersebut harus mendefinisikan lagi TiketKonser
    // namun sesuai dengan nama dan harga masing-masing.
    // Kemudian implements disini berarti untuk semua kelas turunan dari kelas abstrak TiketKonser,
    // harus mengimplementasikan method getHargaTiket() yang ada pada interface HargaTiket.
    private String nama;
    private int harga;

    public TiketKonser(String nama, int harga) {
        this.nama = nama;
        this.harga = harga;
    }

    public String getNama() {
        return nama;
    }

    public int getHarga() {
        return harga;
    }
}